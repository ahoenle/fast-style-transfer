#!/bin/bash -l
#SBATCH -o ./job.out.%j
#SBATCH -e ./job.err.%j
#SBATCH -D ./
#SBATCH -J neuralstyle
#SBATCH --partition=gpu
#SBATCH --constraint="gpu"
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=32
#SBATCH --mail-type=none
# wall clock limit:
#SBATCH --time=10:00:00

module purge
#module load anaconda/3 cuda/8.0 cudnn/6.0 keras/2.0 tensorflow/1.1
#module load anaconda/3/4.3.1 cuda/8.0 cudnn/5.1 theano/0.9 keras/2.0
